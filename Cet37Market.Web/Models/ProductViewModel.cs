﻿namespace Cet37Market.Web.Models
{
    using System.ComponentModel.DataAnnotations;
    using Cet37Market.Web.Data.Entities;
    using Microsoft.AspNetCore.Http;
    

    public class ProductViewModel:Product
    {
        [Display(Name="Image")]
        public IFormFile ImageFile { get; set; }
    }
}
