﻿namespace Cet37Market.Web.Data
{
    using Entities;

    public interface ICountryRepository :IGenericRepository<Country>
    {

    }
}
