﻿namespace Cet37Market.UIForms.ViewModels
{
    using System.Windows.Input;
    using Common.Models;
    using Views;
    using GalaSoft.MvvmLight.Command;
   

    public class ProductItemViewModel : Product
    {
        public ICommand SelectProductCommand => new RelayCommand(this.SelectProduct);

        private async void SelectProduct()
        {
            MainViewModel.GetInstance().EditProduct = new EditProductViewModel((Product)this);
            await App.Navigator.PushAsync(new EditProductPage());
        }
    }
}
