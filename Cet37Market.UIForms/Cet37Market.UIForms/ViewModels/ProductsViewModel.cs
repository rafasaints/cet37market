﻿namespace Cet37Market.UIForms.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using Common.Models;
    using Common.Services;
    using Xamarin.Forms;

    public class ProductsViewModel:BaseViewModel
    {
        #region Attributes

        private ApiService apiService;
        private NetService netService;
        private List<Product> myProducts;

        private ObservableCollection<ProductItemViewModel> products;

        private bool isRefreshing;
      

        #endregion


        #region Properties

        public ObservableCollection<ProductItemViewModel> Products
        {
            get => this.products;
            set => this.SetValue(ref this.products, value);
        }


        public bool IsRefreshing
        {
            get => this.isRefreshing;
            set => this.SetValue(ref this.isRefreshing, value);
        }

        #endregion


        public ProductsViewModel()
        {
            this.apiService = new ApiService();
            this.netService = new NetService();
            this.LoadProducts();
        }

        private async void LoadProducts()
        {
            this.IsRefreshing = true;

            var connection = await this.netService.CheckConnection();
            if (!connection.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "Accept");
                return;
            }

            var url = Application.Current.Resources["UrlAPI"].ToString();
            var response = await this.apiService.GetListAsync<Product>(
                url,
                "/api",
                "/Products",
                "bearer",
                MainViewModel.GetInstance().Token.Token);

            this.IsRefreshing = false;

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");

                return;
            }

            this.myProducts = (List<Product>)response.Result;
            this.RefreshProductList();
            
        }

        public void AddProductsToList(Product product)
        {
            this.myProducts.Add(product);
            this.RefreshProductList();
        }

        public void UpdateProductInList(Product product)
        {
            var previousProduct = this.myProducts.Where(p => p.Id == product.Id).FirstOrDefault();
            if(previousProduct != null)
            {
                this.myProducts.Remove(previousProduct);
            }

            this.myProducts.Add(product);
            this.RefreshProductList();
        }

        public void DeleteProductInList(int productId)
        {
            var previousProduct = this.myProducts.Where(p => p.Id == productId).FirstOrDefault();
            if (previousProduct != null)
            {
                this.myProducts.Remove(previousProduct);
            }

            this.RefreshProductList();
        }

        private void RefreshProductList()
        {
            this.Products = new ObservableCollection<ProductItemViewModel>(
                this.myProducts.Select(p => new ProductItemViewModel
                {
                    Id = p.Id,
                    ImageUrl = p.ImageUrl,
                    ImageFullPath = p.ImageFullPath,
                    IsAvailable = p.IsAvailable,
                    LastPurchase = p.LastPurchase,
                    LastSale = p.LastSale,
                    Name = p.Name,
                    Price = p.Price,
                    Stock = p.Stock,
                    User = p.User
                })
                .OrderBy(p => p.Name)
                .ToList());
        }
    }
}
