﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Cet37Market.UIForms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MasterPage : MasterDetailPage

	{
		public MasterPage ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.Navigator = this.Navigator;
            App.Master = this;
        }
    }
}